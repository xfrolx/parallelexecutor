﻿using System.Collections.Concurrent;
using ParallelExecutor.Client;
using Polly;

namespace ParallelExecutor;

public static class ClientExtensions
{
    public static async Task<Response[]> GetResponses_AsyncForeach(this IClient client, long[] ids, int maxDegreeParallelism)
    {
        var result = new List<Response>(ids.Length);
        await Parallel.ForEachAsync(ids, new ParallelOptions
        {
            MaxDegreeOfParallelism = maxDegreeParallelism
        }, async (id, _) =>
        {
            var response = await client.GetResultAsync(id);
            lock (result)
            {
                result.Add(response);
            }
        });

        return result.ToArray();
    }
    
    public static async Task<Response[]> GetResponses_AsyncForeach_ConcurrentBag(this IClient client, long[] ids, int maxDegreeParallelism)
    {
        var result = new ConcurrentBag<Response>();
        await Parallel.ForEachAsync(ids, new ParallelOptions
        {
            MaxDegreeOfParallelism = maxDegreeParallelism
        }, async (id, _) =>
        {
            var response = await client.GetResultAsync(id);
            result.Add(response);
        });

        return result.ToArray();
    }

    public static async Task<Response[]> GetResponses_Semaphore(this IClient client, long[] ids, int maxDegreeParallelism)
    {
        using var semaphore = new SemaphoreSlim(maxDegreeParallelism);
        var tasks = ids.Select(async id =>
        {
            await semaphore.WaitAsync();
            try
            {
                return await client.GetResultAsync(id);
            }
            finally
            {
                semaphore.Release();
            }
        }).ToList();

        return await Task.WhenAll(tasks);
    }

    public static async Task<Response[]> GetResponses_Chunks(this IClient client, long[] ids, int maxDegreeParallelism)
    {
        var result = new List<Response>();
        foreach (var chunk in ids.Chunk(maxDegreeParallelism))
        {
            result.AddRange(await Task.WhenAll(chunk.Select(client.GetResultAsync)));
        }

        return result.ToArray();
    }
    
    public static async Task<Response[]> GetResponses_Polly(this IClient client, long[] ids, int maxDegreeOfParallelism)
    {
        var result = new List<Response>(ids.Length);
        var policy = Policy
            .BulkheadAsync(maxDegreeOfParallelism, maxDegreeOfParallelism);

        foreach (var chunk in ids.Chunk(maxDegreeOfParallelism))
        {
            await Task.WhenAll(chunk
                .Select(id => policy
                    .ExecuteAsync(async () =>
                        {
                            var response = await client.GetResultAsync(id);
                            lock (result)
                            {
                                result.Add(response);
                            }
                        }
                    )));
        }

        return result.ToArray();
    }

    public static async Task<Response[]> GetResponses_Workers_Lock(this IClient client, long[] ids, int maxDegreeOfParallelism)
    {
        var queue = new ConcurrentQueue<long>(ids);
        var result = new List<Response>(ids.Length);
        var workers = new Task[maxDegreeOfParallelism];
        
        async Task Func()
        {
            while (queue.TryDequeue(out var id))
            {
                var response = await client.GetResultAsync(id);
                lock (result)
                {
                    result.Add(response);
                }
            }
        }

        for (var i = 0; i < maxDegreeOfParallelism; i++)
        {
            workers[i] = Func();
        }

        await Task.WhenAll(workers);

        return result.ToArray();
    }

    public static async Task<Response[]> GetResponses_Workers_Lists(this IClient client, long[] ids, int maxDegreeOfParallelism)
    {
        var queue = new ConcurrentQueue<long>(ids);
        var workers = new Task<List<Response>>[maxDegreeOfParallelism];
        
        async Task<List<Response>> Func()
        {
            var resultChunk = new List<Response>(ids.Length / maxDegreeOfParallelism + 1);
            while (queue.TryDequeue(out var id))
            {
                var response = await client.GetResultAsync(id);
                resultChunk.Add(response);
            }

            return resultChunk;
        }

        for (var i = 0; i < maxDegreeOfParallelism; i++)
        {
            workers[i] = Func();
        }

        var result = await Task.WhenAll(workers);
        return result.SelectMany(x => x).ToArray();
    }
}