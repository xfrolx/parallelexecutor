﻿using BenchmarkDotNet.Attributes;
using ParallelExecutor.Client;

namespace ParallelExecutor;

[MemoryDiagnoser]
public class Benchmarks
{
    private readonly IClient _client = new Client.Client();
    private readonly long[] _ids = Enumerable.Range(0, 1000).Select(x => (long)x).ToArray();
    
    [Params(1, 5, 10, 20, 50)]
    public int MaxDegreeParallelism { get; set; }
    
    [Benchmark]
    public Task GetResponses_AsyncForeach()
    {
        return _client.GetResponses_AsyncForeach(_ids, MaxDegreeParallelism);
    }
    
    [Benchmark]
    public Task GetResponses_AsyncForeach_ConcurrentBag()
    {
        return _client.GetResponses_AsyncForeach_ConcurrentBag(_ids, MaxDegreeParallelism);
    }
    
    [Benchmark]
    public Task GetResponses_Semaphore()
    {
        return _client.GetResponses_Semaphore(_ids, MaxDegreeParallelism);
    }
    
    [Benchmark]
    public Task GetResponses_Chunks()
    {
        return _client.GetResponses_Chunks(_ids, MaxDegreeParallelism);
    }
    
    [Benchmark]
    public Task GetResponses_Polly()
    {
        return _client.GetResponses_Polly(_ids, MaxDegreeParallelism);
    }

    [Benchmark]
    public Task GetResponses_Workers_Lock()
    {
        return _client.GetResponses_Workers_Lock(_ids, MaxDegreeParallelism);
    }

    [Benchmark]
    public Task GetResponses_Workers_Lists()
    {
        return _client.GetResponses_Workers_Lists(_ids, MaxDegreeParallelism);
    }
}