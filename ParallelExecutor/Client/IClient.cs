namespace ParallelExecutor.Client;

public interface IClient
{
    Task<Response> GetResultAsync(long id);
}