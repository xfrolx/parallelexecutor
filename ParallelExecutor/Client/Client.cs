﻿namespace ParallelExecutor.Client;

public sealed class Client : IClient
{
    public async Task<Response> GetResultAsync(long id)
    {
        await Task.Yield();
        return new Response
        {
            Id = id
        };
    }
}