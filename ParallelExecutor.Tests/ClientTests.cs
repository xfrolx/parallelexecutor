﻿using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ParallelExecutor.Tests;

public sealed class ClientTests
{
    private readonly long[] _ids = Enumerable.Range(0, 1000).Select(x => (long)x).ToArray();

    [Fact]
    public async Task Test_Chunks()
    {
        var testClient = new TestClient(new Client.Client(), 5);
        var responses = await testClient.GetResponses_Chunks(_ids, 5);
        Assert.True(responses.Select(x => x.Id).SequenceEqual(_ids));
        await Assert.ThrowsAsync<ParallelExecutionException>(() => testClient.GetResponses_Chunks(_ids, 7));
    }

    [Fact]
    public async Task Test_Polly()
    {
        var testClient = new TestClient(new Client.Client(), 5);
        var responses = await testClient.GetResponses_Polly(_ids, 5);
        Assert.True(responses.Select(x => x.Id).OrderBy(x => x).SequenceEqual(_ids));
        await Assert.ThrowsAsync<ParallelExecutionException>(() => testClient.GetResponses_Polly(_ids, 7));
    }

    [Fact]
    public async Task Test_Semaphore()
    {
        var testClient = new TestClient(new Client.Client(), 5);
        var responses = await testClient.GetResponses_Semaphore(_ids, 5);
        Assert.True(responses.Select(x => x.Id).SequenceEqual(_ids));
        await Assert.ThrowsAsync<ParallelExecutionException>(() => testClient.GetResponses_Semaphore(_ids, 7));
    }

    [Fact]
    public async Task Test_AsyncForeach()
    {
        var testClient = new TestClient(new Client.Client(), 5);
        var responses = await testClient.GetResponses_AsyncForeach(_ids, 5);
        Assert.True(responses.Select(x => x.Id).OrderBy(x => x).SequenceEqual(_ids));
        await Assert.ThrowsAsync<ParallelExecutionException>(() => testClient.GetResponses_AsyncForeach(_ids, 7));
    }

    [Fact]
    public async Task Test_Workers()
    {
        var testClient = new TestClient(new Client.Client(), 5);
        var responses = await testClient.GetResponses_Workers_Lock(_ids, 5);
        Assert.True(responses.Select(x => x.Id).OrderBy(x => x).SequenceEqual(_ids));
        await Assert.ThrowsAsync<ParallelExecutionException>(() => testClient.GetResponses_Workers_Lock(_ids, 7));
    }
}