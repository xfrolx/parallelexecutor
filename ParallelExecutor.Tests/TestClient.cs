﻿using System.Threading;
using System.Threading.Tasks;
using ParallelExecutor.Client;

namespace ParallelExecutor.Tests;

public sealed class TestClient: IClient
{
    private readonly IClient _client;
    private readonly int _maxParallelExecutingRequests;
    private int _executingRequests;
    public TestClient(IClient client, int maxParallelExecutingRequests)
    {
        _client = client;
        _maxParallelExecutingRequests = maxParallelExecutingRequests;
    }
    public async Task<Response> GetResultAsync(long id)
    {
        Interlocked.Increment(ref _executingRequests);
        
        if (_executingRequests > _maxParallelExecutingRequests)
        {
            throw new ParallelExecutionException();
        }
        
        try
        {
            await Task.Delay(10);
            return await _client.GetResultAsync(id);
        }
        finally
        {
            Interlocked.Decrement(ref _executingRequests);
        }
    }
}